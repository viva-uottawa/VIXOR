"""
Copyright 2019-2020 Selameab (https://github.com/Selameab)
"""

import numpy as np

# Physical space dimensions
PH_WIDTH, PH_HEIGHT, PH_DEPTH = 70, 80, 3.5


# Quantizes points from physical space into cube space
class OccupancyCuboid:
    def __init__(self, shape):
        self.grid_height, self.grid_width, self.grid_depth = shape

        # Quantization factor(x, y, z) = grid dimensions / physical dimension  (Eg. 800 / 80 = 10)
        self.qf = self.grid_width / PH_WIDTH, self.grid_height / PH_HEIGHT, self.grid_depth / PH_DEPTH

    def encode(self, velo):
        pts, _ = velo
        # pts are in physical space; (ix, iy, iz) are in grid space
        ix = ((pts[2]) * self.qf[0]).astype(np.int32)
        iy = ((pts[0] + 40) * self.qf[1]).astype(np.int32)
        iz = ((pts[1] + 1) * self.qf[2]).astype(np.int32)

        occupancy_grid = np.zeros(shape=(self.grid_height, self.grid_width, self.grid_depth), dtype=np.float32)
        occupancy_grid[iy, ix, iz] = 1
        return occupancy_grid

    def encode_batch(self, pc_batch):
        return np.array([self.encode(pc) for pc in pc_batch])


def main():
    from kitti.reader import Reader, CARS_ONLY
    import matplotlib.pyplot as plt

    reader = Reader(CARS_ONLY)
    encoder = OccupancyCuboid(shape=(800, 700, 35))
    target = encoder.encode(reader.get_velo('000032'))
    print(target.shape)
    plt.imshow(np.sum(target, axis=-1) > 0)
    plt.show()


if __name__ == '__main__':
    main()
