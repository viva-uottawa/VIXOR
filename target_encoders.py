"""
Copyright 2019-2020 Selameab (https://github.com/Selameab)
"""

import numpy as np
from kitti.boxes import Box3D, get_corners_3D

# Physical space dimensions
P_WIDTH, P_HEIGHT, P_DEPTH = 70, 80, 3.5


class PixorTargets:
    def __init__(self, shape, target_means, target_stds, default_class='Car'):
        self.target_height, self.target_width = shape
        self.target_means, self.target_stds = target_means, target_stds
        self.default_class = default_class

        # Quantization factor(x, y) = target dimensions / physical dimensions (Eg. 800 / 80 = 10)
        self.qf = np.asarray([self.target_width / P_WIDTH, self.target_height / P_HEIGHT], dtype=np.float32)

        xx, yy = np.meshgrid(range(0, self.target_width), range(0, self.target_height))
        self.feature_map_pts = np.vstack((xx.ravel(), yy.ravel()))

    # Generates target map for a single 3D bounding box
    def __generate_map(self, box_3D):
        # Get 4 corners of the rectangle in BEV and transform to feature coordinate frame
        corners = np.array(get_corners_3D(box_3D)[[2, 0], 0:4])  # Shape: 2 (#coordinates/z,x), 4(#corners),
        corners[1] += 40  # Move RF to the top
        corners = self.qf[:, np.newaxis] * corners  # Convert from physical space (80 x 70m) to feature space (200 x 175px)

        # Compute Objectness map
        V01, V12 = corners[:, 1] - corners[:, 0], corners[:, 2] - corners[:, 1]
        V0P, V1P = self.feature_map_pts - corners[:, 0, np.newaxis], self.feature_map_pts - corners[:, 1, np.newaxis]
        proj_0P_on_01, proj_1P_on_12 = np.dot(V01, V0P), np.dot(V12, V1P)
        mag_01, mag_12 = np.dot(V01, V01), np.dot(V12, V12)
        obj_map = ((0 <= proj_0P_on_01) & (proj_0P_on_01 <= mag_01) & (0 <= proj_1P_on_12) & (proj_1P_on_12 <= mag_12))

        # Generate corresponding geometry map for pts in rectangle
        # cos, sin, z, x, width, length, y, h, anchor_state(0 - negative, 1 - positive)
        # y, h - are not regressed in original paper

        geo_map = np.zeros(shape=(self.target_height, self.target_width, 9), dtype=np.float32)
        width, length, height = np.math.log(box_3D.w), np.math.log(box_3D.l), np.math.log(box_3D.h)
        cos, sin = np.math.cos(box_3D.yaw), np.math.sin(box_3D.yaw)
        physical_y = box_3D.y
        for positive_pt in self.feature_map_pts.T[obj_map]:
            # Angle
            geo_map[positive_pt[1], positive_pt[0], (0, 1)] = cos, sin

            # Offset from center
            physical_z = positive_pt[0] / self.qf[0]  # Convert to physical space
            physical_x = positive_pt[1] / self.qf[1] - 40  # Convert to physical space and move RF from top to middle
            geo_map[positive_pt[1], positive_pt[0], (2, 3)] = ((physical_z - box_3D.z), (physical_x - box_3D.x))

            # Width and Length
            geo_map[positive_pt[1], positive_pt[0], (4, 5)] = width, length

            # y and height
            geo_map[positive_pt[1], positive_pt[0], (6, 7)] = physical_y, height

            # Normalize
            geo_map[positive_pt[1], positive_pt[0], :-1] -= self.target_means
            geo_map[positive_pt[1], positive_pt[0], :-1] /= self.target_stds

            # Anchor state
            geo_map[positive_pt[1], positive_pt[0], -1] = 1

        obj_map = obj_map.astype(np.float32).reshape(self.target_height, self.target_width, 1)

        return obj_map, geo_map

    def encode(self, boxes_3D):
        obj_map = np.zeros(shape=(200, 175, 1), dtype=np.float32)
        geo_map = np.zeros(shape=(200, 175, 9), dtype=np.float32)
        for box_3D in boxes_3D:
            _obj, _geo = self.__generate_map(box_3D)
            obj_map += _obj
            geo_map += _geo

        return obj_map, geo_map

    def encode_batch(self, boxes_3D_batch):
        batch_size = len(boxes_3D_batch)
        obj_map = np.zeros(shape=(batch_size, 200, 175, 1), dtype=np.float32)
        geo_map = np.zeros(shape=(batch_size, 200, 175, 9), dtype=np.float32)

        for i in range(batch_size):
            obj_map[i], geo_map[i] = self.encode(boxes_3D_batch[i])

        return obj_map, geo_map

    def decode(self, obj_map, geo_map, confidence_thresh=0.1):
        boxes_3D = []
        obj_map_flat = obj_map.flatten()
        for positive_pt in self.feature_map_pts.T[obj_map_flat > confidence_thresh]:
            # Denormalize
            geo_map[positive_pt[1], positive_pt[0]] *= self.target_stds
            geo_map[positive_pt[1], positive_pt[0]] += self.target_means

            # Offset
            z = positive_pt[0] / self.qf[0]  # Convert to physical space
            x = positive_pt[1] / self.qf[1] - P_HEIGHT / 2  # Convert to physical space and move reference frame from top to middle
            z -= geo_map[positive_pt[1], positive_pt[0], 2]
            x -= geo_map[positive_pt[1], positive_pt[0], 3]

            # Size
            w, l, h = np.exp(geo_map[positive_pt[1], positive_pt[0], (4, 5, 7)])

            # Angle
            yaw = np.math.atan2(geo_map[positive_pt[1], positive_pt[0], 1], geo_map[positive_pt[1], positive_pt[0], 0])

            # y
            y = geo_map[positive_pt[1], positive_pt[0], 6]

            decoded_box = Box3D(h=h, w=w, l=l,
                                x=x, y=y, z=z,
                                yaw=yaw, confidence=obj_map[positive_pt[1], positive_pt[0], 0],
                                cls=self.default_class)
            boxes_3D += [decoded_box]
        return boxes_3D

    def decode_batch(self, pred_batch, confidence_thresh=0.1):
        obj_batch, geo_batch = pred_batch  # Split
        boxes_3D_batch = []
        for i in range(len(obj_batch)):
            boxes_3D_batch += [self.decode(obj_batch[i], geo_batch[i], confidence_thresh)]

        return boxes_3D_batch


def main():
    from kitti.reader import Reader, CARS_ONLY
    from kitti import visualizers as kv
    import matplotlib.pyplot as plt

    reader = Reader(CARS_ONLY)
    TARGET_MEANS = np.array([0.0, 0.0, 0.0, 0.0, 0.48, 1.36, 1.75, 0.42])
    TARGET_STDS = np.array([0.46, 0.88, 1.03, 0.68, 0.06, 0.11, 0.36, 0.08])
    encoder = PixorTargets(shape=(200, 175), target_means=TARGET_MEANS, target_stds=TARGET_STDS)

    t = '000432'
    gt_boxes = reader.get_boxes_3D(t)
    obj_map, geo_map = encoder.encode(gt_boxes)

    # View maps
    fig, ax = plt.subplots(nrows=1, ncols=geo_map.shape[-1])
    for i in range(geo_map.shape[-1]):
        ax[i].imshow(geo_map[..., i])
    plt.show()

    # Verify decoder
    decoded_boxes = encoder.decode(obj_map, geo_map[..., :-1])
    print("GT Count =", len(gt_boxes))
    print("Decoded Count =", len(decoded_boxes))
    fig, ax = plt.subplots(nrows=1, ncols=2)
    ax[0].imshow(kv.bev(gt_boxes=gt_boxes))
    ax[1].imshow(kv.bev(gt_boxes=decoded_boxes))
    plt.show()


if __name__ == '__main__':
    main()
