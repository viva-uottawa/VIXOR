# VIXOR
This is a reimplementation of PIXOR (https://arxiv.org/abs/1902.06326) and PIXOR++ (http://proceedings.mlr.press/v87/yang18b/yang18b.pdf) at VIVA Lab.

Getting Started
----------------
1) Download the KITTI Dataset ([point cloud](http://www.cvlibs.net/download.php?file=data_object_velodyne.zip), [annotations](http://www.cvlibs.net/download.php?file=data_object_label_2.zip), [calibration matrices](http://www.cvlibs.net/download.php?file=data_object_calib.zip) and [left color images](http://www.cvlibs.net/download.php?file=data_object_image_2.zip) (_used only for visualization_)).
2) Extract the zips to create a folder structure similar to the one below.
    ```
   training
    ├───calib
    ├───image_2
    ├───label_2
    └───velodyne
    ```
    
3) Clone this repository  
    ```shell
    git clone https://gitlab.com/Selameab/VIXOR.git
    ```
4) Change `KITTI_DIR` in `kitti/reader.py` to point to the extracted dataset.
5) Install dependencies 
    ```shell
    pip3 install -r requirements.txt
    ```
6) Compile KITTI evaluation code
    ```
    cd kitti/kitti_eval
    g++ -o evaluate_object_3d_offline evaluate_object_3d_offline.cpp
    chmod +x evaluate_object_3d_offline
    ```
Training
-------
Run 
```shell
python main.py train
```
The trained model, sample predictions, evaluation reports, etc will be stored in `outputs`. 

Inference
-------
Pretrained weights can be downloaded from [here](https://drive.google.com/file/d/1gOhfDCcP-fE-vk1CW4zTO3vGJhc9P6aD/view?usp=sharing). 

Run
```shell
python main.py predict -c path/to/trained/model
```
Enter `frame id` (0 - 7480) of the frame to test or `'q'` to quit. 

Results
-------

AP on KITTI BEV Benchmark.

|         | Easy | Moderate | Hard |
|---------|------|----------|------|
| **PIXOR**   | 81.7 | 77.0     | 72.9 |
| **PIXOR++** | 89.4 | 83.7     | 77.9 |
| **VIXOR***   | 88.1 | 81.4     | 79.5 |

\* - <sub><sup>Evaluated on MV3D validation split</sup></sub>

Predictions in front view
![](images/front_53.png)

Predictions in bird's eye view
![](images/bev_53.png)

Predictions in 3D
![](images/open3d_53.png)


Contributors
-------
* Selameab S. Demilew
* Hamed H. Aghdam
* Yahya Massoud
* Robert Laganière


