"""
Copyright 2019-2020 Selameab (https://github.com/Selameab)
"""

import tensorflow as tf

_EPSILON = tf.keras.backend.epsilon()


def focal_loss(alpha, gamma):
    def focal_loss_fixed(y_true, y_pred):
        pos_mask = tf.cast(tf.equal(y_true, 1.0), tf.float32)
        neg_mask = tf.cast(tf.less(y_true, 1.0), tf.float32)

        pos_loss = -tf.math.log(tf.clip_by_value(y_pred, _EPSILON, 1.0 - _EPSILON)) * tf.pow(1 - y_pred, gamma) * pos_mask
        neg_loss = -tf.math.log(tf.clip_by_value(1 - y_pred, _EPSILON, 1.0 - _EPSILON)) * tf.pow(y_pred, gamma) * neg_mask

        num_pos = tf.reduce_sum(pos_mask)
        pos_loss = tf.reduce_sum(pos_loss) * alpha
        neg_loss = tf.reduce_sum(neg_loss) * (1 - alpha)

        return (pos_loss + neg_loss) / (num_pos + 1)

    return focal_loss_fixed


def geo_loss(y_true, y_pred):
    mask = tf.equal(y_true[:, :, :, -1], 1.0)
    pos_count = tf.reduce_sum(tf.cast(mask, tf.float32))
    y_true = y_true[:, :, :, 0:-1]

    y_true = tf.boolean_mask(y_true, mask)
    y_pred = tf.boolean_mask(y_pred, mask)

    return tf.reduce_sum(tf.abs(y_true - y_pred)) / (pos_count + 1)
