import tensorflow as tf
from tensorflow.keras.layers import Conv2D, Input, Concatenate, BatchNormalization, MaxPooling2D, AveragePooling2D, ReLU, Add
from tensorflow.keras.models import Model


def create_model(input_shape, kr=None, ki='he_normal'):
    def _cbr(p, filters, name, batch_norm=True):
        with tf.name_scope(name):
            p = Conv2D(filters, (3, 3), activation=None, padding='same', use_bias=not batch_norm, kernel_regularizer=kr, kernel_initializer=ki)(p)
            if batch_norm:
                p = BatchNormalization()(p)

            p = ReLU()(p)
        return p

    def _residual(p, filters, name, downsample=False):
        with tf.name_scope(name):
            q = Conv2D(filters[0], (1, 1), activation=None, padding='same', use_bias=False, kernel_regularizer=kr, kernel_initializer=ki)(p)
            q = BatchNormalization()(q)
            q = ReLU()(q)

            q = Conv2D(filters[1], (3, 3), activation=None, padding='same', strides=(2 if downsample else 1), use_bias=False, kernel_regularizer=kr, kernel_initializer=ki)(q)
            q = BatchNormalization()(q)
            q = ReLU()(q)

            q = Conv2D(filters[2], (1, 1), activation=None, padding='same', use_bias=False, kernel_regularizer=kr, kernel_initializer=ki)(q)
            q = BatchNormalization()(q)

            r = p
            if downsample:
                r = Conv2D(filters[2], (1, 1), activation=None, padding='same', strides=2, use_bias=False, kernel_regularizer=kr, kernel_initializer=ki)(p)
                q = BatchNormalization()(q)

            q = Add()([q, r])
            return ReLU()(q)

    def _shortcut(p, filters, name):
        with tf.name_scope(name):
            return Conv2D(filters, (1, 1), activation=None, padding='same', use_bias=True, kernel_regularizer=kr, kernel_initializer=ki)(p)

    def _deconv(p, filters, name, target_shape):
        with tf.name_scope(name):
            p = tf.image.resize(p, size=target_shape)
            return Conv2D(filters, (3, 3), activation=None, padding='same', use_bias=True, kernel_regularizer=kr, kernel_initializer=ki)(p)

    tf.keras.backend.clear_session()
    input_tensor = Input(shape=input_shape)
    x = input_tensor

    # Block 1
    with tf.name_scope('Block_1'):
        x = _cbr(x, 32, "C1")
        x = _cbr(x, 32, "C2")

    # Block 2 (3 Layers)
    with tf.name_scope('Block_2'):
        for i in range(1, 4):
            x = _residual(x, [24, 24, 96], f"R{i}", downsample=(i == 1))

    # Block 3 (6 Layers)
    with tf.name_scope('Block_3'):
        for i in range(1, 7):
            x = _residual(x, [48, 48, 96], f"R{i}", downsample=(i == 1))
        b3_shortcut = _shortcut(x, 96, "S3")

    # Block 4 (6 Layers)
    with tf.name_scope('Block_4'):
        for i in range(1, 7):
            x = _residual(x, [64, 64, 256], f"R{i}", downsample=(i == 1))
        b4_shortcut = _shortcut(x, 128, "S4")

    # Block 5 (3 Layers)
    with tf.name_scope('Block_5'):
        for i in range(1, 4):
            x = _residual(x, [96, 96, 384], f"R{i}", downsample=(i == 1))
        b5_shortcut = _shortcut(x, 196, "S5")

    # Upsample 6
    with tf.name_scope('Upsample_6'):
        x = _deconv(b5_shortcut, 128, "D", target_shape=b4_shortcut.shape[1:3])
        x = Add()([x, b4_shortcut])

    # Upsample 7
    with tf.name_scope('Upsample_7'):
        x = _deconv(x, 96, "D", target_shape=b3_shortcut.shape[1:3])
        x = Add()([x, b3_shortcut])

    # Head
    with tf.name_scope('Head'):
        x = _cbr(x, 96, "C1")
        for i in range(2, 4):
            x = _cbr(x, 96, f"C{i}", batch_norm=False)

        obj_map = Conv2D(1, kernel_size=(3, 3), padding='same', activation='sigmoid', name='obj_map', kernel_regularizer=kr, kernel_initializer='glorot_normal')(x)
        geo_map = Conv2D(8, kernel_size=(3, 3), padding='same', activation=None, name='geo_map', kernel_regularizer=kr, kernel_initializer='glorot_normal')(x)

    return Model(inputs=input_tensor, outputs=[obj_map, geo_map])


def main():
    tbc = tf.keras.callbacks.TensorBoard(log_dir="logs")
    model = create_model((800, 700, 35))
    tbc.set_model(model)


if __name__ == '__main__':
    main()
