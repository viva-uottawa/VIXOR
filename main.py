"""
Copyright 2019-2020 Selameab (https://github.com/Selameab)
"""

import argparse
import os
from datetime import datetime

import numpy as np
from tensorflow.keras.callbacks import ModelCheckpoint, LearningRateScheduler
from tensorflow.keras.optimizers import Adam

from kitti import visualizers as kv
from kitti.evaluator import Evaluator
from kitti.generators import Generator
from kitti.kitti_utils import nms_bev_dist
from kitti.reader import CARS_ONLY
from kitti.reader import Reader
from kitti.velo_augmentations import per_box_dropout, per_box_rotation_translation, flip_along_y, global_scale, global_rot, global_trans
from losses import focal_loss, geo_loss
from models.pixor_pp import create_model
from target_encoders import PixorTargets
from velo_encoders import OccupancyCuboid

CLASS_DICT = CARS_ONLY


def parse_args():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help='train / predict', dest='action')
    subparsers.required = True

    subparsers.add_parser('train')

    predict_parser = subparsers.add_parser('predict')
    predict_parser.add_argument('-c', '--ckpt', help='Path to trained model', required=True)

    return parser.parse_args()


def scheduler(epoch):
    epoch = epoch + 1
    if epoch <= 1:  # Warm-up
        lr = 1e-5
    elif epoch < 10:
        lr = 1e-3
    elif epoch < 40:
        lr = 1e-4
    else:
        lr = 1e-5
    print(f"\nEpoch {epoch} - Setting learning rate to {lr}")
    return lr


def train(model, velo_encoder, target_encoder):
    # Generators
    train_gen_with_aug = Generator('train', CLASS_DICT, velo_encoder=velo_encoder, boxes_3D_encoder=target_encoder, batch_size=6,
                                   velo_augmentations=[per_box_dropout(0.1), flip_along_y(),
                                                       per_box_rotation_translation(rot_range=(-np.pi/15, np.pi/15), trans_range=((-2, 2), (-0.1, 0.1), (-2, 2))),
                                                       global_scale((0.95, 1.05)), global_rot((-0.05, 0.05)), global_trans(((-2, 2), (-0.5, 0.5), (-3, 3))),
                                                       None, None, None, None])  # None = No augmentation  (i.e. 0.6 chance of augmentation)
    train_gen = Generator('train', CLASS_DICT, velo_encoder=velo_encoder, boxes_3D_encoder=target_encoder, batch_size=6)
    val_gen = Generator('val', CLASS_DICT, velo_encoder=velo_encoder, boxes_3D_encoder=target_encoder, batch_size=6)

    # Callbacks
    outputs_dir = os.path.join('outputs', datetime.now().strftime('%y%m%d_%H%M'))
    ckpts_dir = os.path.join(outputs_dir, 'ckpts')
    os.makedirs(ckpts_dir, exist_ok=True)
    callbacks = [LearningRateScheduler(scheduler),
                 ModelCheckpoint(os.path.join(ckpts_dir, 'E{epoch:04d}.h5'), save_best_only=False, period=5, verbose=True, save_weights_only=True),
                 Evaluator(outputs_dir, train_gen, val_gen, 5, target_encoder.decode_batch, nms_bev_dist(thresh=-1.0))]

    # Train
    model.compile(optimizer=Adam(learning_rate=1e-3), loss=[focal_loss(alpha=0.75, gamma=1.0), geo_loss])
    model.fit(train_gen_with_aug, validation_data=val_gen, epochs=150, callbacks=callbacks, validation_freq=5, max_queue_size=20, use_multiprocessing=False, workers=8)


def predict(model, velo_encoder, target_encoder, weights_path):
    # Load weights
    model.load_weights(weights_path)

    # Reader
    reader = Reader(CLASS_DICT)

    while True:
        t = input("Frame ID [0 - 7480; 'q' to exit]:> ")

        # Exit point
        if t in ['q', 'Q']:
            exit()

        t = "%06d" % int(t)

        velo = reader.get_velo(t)
        img = reader.get_image(t)
        gt_boxes = reader.get_boxes_3D(t)
        P2 = reader.get_calib(t)[2]

        # Predict
        y_pred = model.predict(velo_encoder.encode_batch([velo]))
        pred_boxes = target_encoder.decode_batch(y_pred, 0.5)[0]
        pred_boxes = nms_bev_dist(thresh=-1.0)(pred_boxes)

        # Add confidence as text
        for b in pred_boxes:
            b.text = f"{b.confidence:.2f}"

        # Visualize
        kv.imshow(np.concatenate(
            (kv.bev(velo[0], pred_boxes=gt_boxes, title="GT"),
             kv.bev(velo[0], pred_boxes=pred_boxes, title="Det")), axis=1),
            block=False)  # BEV
        kv.imshow(kv.range_view(img, P2, pred_boxes=pred_boxes), block=False)  # Range View
        kv.open3d(velo[0], pred_boxes=pred_boxes)  # 3D


def main():
    args = parse_args()

    # Encoders
    TARGET_MEANS = np.array([0.0, 0.0, 0.0, 0.0, 0.48, 1.36, 1.75, 0.42])
    TARGET_STDS = np.array([0.46, 0.88, 1.03, 0.68, 0.06, 0.11, 0.36, 0.08])
    velo_encoder = OccupancyCuboid(shape=(800, 700, 35))
    target_encoder = PixorTargets(shape=(200, 175), target_means=TARGET_MEANS, target_stds=TARGET_STDS, default_class='Car')

    # Create model
    model = create_model((800, 700, 35))

    if args.action == 'train':
        train(model, velo_encoder, target_encoder)
    elif args.action == 'predict':
        predict(model, velo_encoder, target_encoder, args.ckpt)


if __name__ == '__main__':
    main()
