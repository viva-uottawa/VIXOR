"""
Copyright 2019-2020 Selameab (https://github.com/Selameab)
"""

import os
import pickle

import numpy as np
import tensorflow as tf
from tqdm import tqdm

from . import visualizers as kv
from .boxes import project_box_3D
from .reader import KITTI_DIR

CLASSES = ['car', 'pedestrian', 'cyclist']
TASKS = {'detection': '2D',
         'detection_3d': '3D',
         'detection_ground': 'BEV',
         'orientation': 'AOS'}


class Evaluator(tf.keras.callbacks.Callback):

    def __init__(self, outputs_dir, train_gen, val_gen, val_freq, decode_batch_fn, nms_fn):
        super(Evaluator, self).__init__()
        self.outputs_dir = outputs_dir

        self.train_gen = train_gen
        self.val_gen = val_gen
        self.val_freq = val_freq
        self.decode_batch_fn = decode_batch_fn
        self.nms_fn = nms_fn

        self.log_dir = os.path.join(outputs_dir, 'logs')
        self.writer = tf.compat.v1.summary.FileWriter(logdir=self.log_dir, graph=tf.compat.v1.keras.backend.get_session().graph)
        self.reader = self.val_gen.reader

    def predict(self, gen):
        boxes_dict = {}
        for batch_id in tqdm(range(gen.__len__()), desc=f'Predicting ({gen.subset}): '):
            batch = gen.__getitem__(batch_id)
            pred_batch = self.model.predict(batch)
            boxes_batch = self.decode_batch_fn(pred_batch)

            for t, boxes in zip(gen.get_ids(batch_id), boxes_batch):
                if self.nms_fn is not None:
                    boxes_dict[t] = self.nms_fn(boxes)
                else:
                    boxes_dict[t] = boxes

        return boxes_dict

    @staticmethod
    def _boxes_to_pred_str(boxes_3D, P):
        lines = []
        for box_3D in boxes_3D:
            box_2D = project_box_3D(P, box_3D)
            lines += [
                f"{box_3D.cls} -1 -1 0 {box_2D.x1:.2f} {box_2D.y1:.2f} {box_2D.x2:.2f} {box_2D.y2:.2f} " +
                f"{box_3D.h:.2f} {box_3D.w:.2f} {box_3D.l:.2f} {box_3D.x:.2f} {box_3D.y:.2f} {box_3D.z:.2f} " +
                f"{box_3D.yaw:.2f} {box_3D.confidence:.2f}\n"
            ]
        return lines

    def _boxes_dict_to_txt(self, boxes_dict, dest_dir):
        os.makedirs(dest_dir, exist_ok=True)  # Make dir
        for t in boxes_dict:
            with open(os.path.join(dest_dir, t + '.txt'), 'w') as txt:
                if len(boxes_dict[t]) > 0:
                    txt.writelines(Evaluator._boxes_to_pred_str(boxes_dict[t], P=self.reader.get_calib(t)[2]))

    @staticmethod
    def _get_kitti_eval_executable():
        return os.path.join(os.path.dirname(os.path.abspath(__file__)), 'kitti_eval', 'evaluate_object_3d_offline')

    @staticmethod
    def _get_gt_dir():
        return os.path.join(KITTI_DIR, 'label_2')

    @staticmethod
    def _parse_kitti_eval(eval_dir):
        evaluation = {}  # evaluation['task_shortname/class/difficulty']

        for task, task_short_name in TASKS.items():
            for cls in CLASSES:
                stat_filename = os.path.join(eval_dir, f"stats_{cls}_{task}.txt")
                if os.path.isfile(stat_filename):
                    values = np.loadtxt(stat_filename)
                    aps = np.sum(values[:, 1:], axis=1) / 40
                    evaluation[f"{task_short_name}/{cls}/easy"] = aps[0]
                    evaluation[f"{task_short_name}/{cls}/moderate"] = aps[1]
                    evaluation[f"{task_short_name}/{cls}/hard"] = aps[2]
                    evaluation[f"{task_short_name}/{cls}/overall"] = np.mean(aps)

        return evaluation

    def evaluate(self, gen, epoch):
        preds_dir = os.path.join(self.outputs_dir, "preds", f"{gen.subset}_E{epoch:04d}")
        os.makedirs(preds_dir, exist_ok=True)

        # Predict
        boxes_dict = self.predict(gen)

        # Pickle
        with open(os.path.join(preds_dir, 'preds.pkl'), 'wb') as handle:
            pickle.dump(boxes_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)

        # Generate txts
        self._boxes_dict_to_txt(boxes_dict, os.path.join(preds_dir, "data"))

        # Generate some images (bev and range)
        imgs_dir = os.path.join(preds_dir, "images")
        os.makedirs(imgs_dir, exist_ok=True)
        for t in list(boxes_dict.keys())[0:100]:
            # Add confidence as text
            for b in boxes_dict[t]:
                b.text = f"{b.confidence:.2f}"

            img = self.reader.get_image(t)
            pts = self.reader.get_velo(t)[0]
            gt_boxes = self.reader.get_boxes_3D(t)
            P2 = self.reader.get_calib(t)[2]

            # BEV
            gt_bev = kv.bev(pts, pred_boxes=gt_boxes, title="GT")
            pred_bev = kv.bev(pts, pred_boxes=boxes_dict[t], title="Det")
            kv.imsave(np.concatenate((gt_bev, pred_bev), axis=1), os.path.join(imgs_dir, f'bev_{t}.png'))

            # Range View
            kv.imsave(kv.range_view(img, P2, pred_boxes=boxes_dict[t]), os.path.join(imgs_dir, f'rv_{t}.png'))

        # Evaluate
        cmd = f'{self._get_kitti_eval_executable()} "{self._get_gt_dir()}" "{os.path.abspath(preds_dir)}"'
        os.system(cmd)

        # Load generated pr curves
        eval_results = self._parse_kitti_eval(preds_dir)

        # Write AP values to text file
        with open(os.path.join(preds_dir, "APs.txt"), 'w') as ap_file:
            for metric, value in eval_results.items():
                ap_file.write(f"{metric} \t {value}\n")

        return eval_results

    def on_epoch_end(self, epoch, logs=None):
        epoch = epoch + 1  # To match epoch number displayed on terminal (i.e - epoch starts at 0)
        print(f"\nEpoch {epoch} completed...")

        # Log losses to tensorboard
        scalar_summaries = []
        for metric, value in logs.items():
            if 'val' in metric:
                scalar_summaries += [tf.compat.v1.Summary.Value(tag="Val/" + metric.replace("val_", ""), simple_value=value)]
            else:
                scalar_summaries += [tf.compat.v1.Summary.Value(tag="Train/" + metric, simple_value=value)]

        # Log AP
        if epoch % self.val_freq == 0:
            for metric, value in self.evaluate(self.train_gen, epoch).items():
                scalar_summaries += [tf.compat.v1.Summary.Value(tag="Train/" + metric, simple_value=value)]

            for metric, value in self.evaluate(self.val_gen, epoch).items():
                scalar_summaries += [tf.compat.v1.Summary.Value(tag="Val/" + metric, simple_value=value)]

        # scalar_summaries
        self.writer.add_summary(tf.compat.v1.Summary(value=scalar_summaries), global_step=epoch)
        self.writer.flush()
