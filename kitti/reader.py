"""
Copyright 2019-2020 Selameab (https://github.com/Selameab)
"""

import copy
import csv
import os
import pickle

import imagesize
import numpy as np
from PIL import Image
from tqdm import tqdm

from .boxes import Box2D, Box3D, CORNER_CORNER
from .kitti_utils import fov_filter, box_filter
from .transforms_3D import transform

# Constants
IMG_WIDTH, IMG_HEIGHT = 1242, 375
KITTI_COLUMN_NAMES = ['Type', 'Truncated', 'Occluded', 'Alpha', 'X1', 'Y1', 'X2', 'Y2', '3D_H', '3D_W', '3D_L', '3D_X', '3D_Y', '3D_Z', 'Rot_Y']

# Dataset path: TODO: Change
KITTI_DIR = 'D:/Datasets/KITTI/training' if os.name == 'nt' else os.path.expanduser('~/datasets/KITTI/training/')

# Commonly used class dicts
CARS_ONLY = {'Car': ['Car']}
PEDS_ONLY = {'Pedestrian': ['Pedestrian']}
CYCS_ONLY = {'Cyclist': ['Cyclist']}

CARS_PEDS = {'Car': ['Car'], 'Pedestrian': ['Pedestrian']}
CARS_PEDS_CYCS = {'Car': ['Car'], 'Pedestrian': ['Pedestrian'], 'Cyclist': ['Cyclist']}

SMALL_OBJECTS = {'Pedestrian': ['Pedestrian'], 'Person_sitting': ['Person_sitting'], 'Cyclist': ['Cyclist']}
ALL_VEHICLES = {'Car': ['Car'], 'Van': ['Van'], 'Truck': ['Truck'], 'Tram': ['Tram'], 'Misc': ['Misc']}
ALL_OBJECTS = {'Car': ['Car'], 'Van': ['Van'], 'Truck': ['Truck'],
               'Pedestrian': ['Pedestrian'], 'Person_sitting': ['Person_sitting'], 'Cyclist': ['Cyclist'],
               'Tram': ['Tram'], 'Misc': ['Misc']}


class Reader:
    def __init__(self, class_dict, ds_dir=KITTI_DIR, invalidate_cache=False):
        self.ds_dir = ds_dir
        self.img2_dir = os.path.join(ds_dir, 'image_2')
        self.label_dir = os.path.join(ds_dir, 'label_2')
        self.velo_dir = os.path.join(ds_dir, 'velodyne')
        self.calib_dir = os.path.join(ds_dir, 'calib')
        self.subsets_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'subsets')
        self.cache_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'label_2_cache.pkl')

        # For grouping similar objects into one class (Eg. Cars and Vans)
        self.class_to_group = {}
        for group, classes in class_dict.items():
            for cls in classes:
                self.class_to_group[cls] = group

        if invalidate_cache or (not os.path.isfile(self.cache_path)):
            self._create_cache()

        self._load_cache()
        self._filter_cache()

    def _create_cache(self):
        print("Creating cache...")
        cache = {}
        for t in tqdm(os.listdir(self.img2_dir)):
            t = t.replace(".png", "")
            # Paths
            img_path = os.path.join(self.img2_dir, t + ".png")
            txt_path = os.path.join(self.label_dir, t + ".txt")
            calib_path = os.path.join(self.calib_dir, t + '.txt')

            w, h = imagesize.get(img_path)

            txt_lbl = _read_txt_file(txt_path)
            boxes_2D, boxes_3D = [], []
            for row in txt_lbl:
                for param in ['X1', 'Y1', 'X2', 'Y2', '3D_H', '3D_W', '3D_L', '3D_X', '3D_Y', '3D_Z', 'Rot_Y']:
                    row[param] = np.round(float(row[param]), 2)

                boxes_2D += [Box2D((row['X1'] * IMG_WIDTH / w, row['Y1'] * IMG_HEIGHT / h,
                                    row['X2'] * IMG_WIDTH / w, row['Y2'] * IMG_HEIGHT / h),
                                   mode=CORNER_CORNER, cls=row['Type'])]

                boxes_3D += [Box3D(row['3D_H'], row['3D_W'], row['3D_L'],
                                   row['3D_X'], row['3D_Y'], row['3D_Z'],
                                   row['Rot_Y'], cls=row['Type'])]

            cache[t] = {'image_size': (w, h),
                        'boxes_2D': boxes_2D,
                        'boxes_3D': boxes_3D,
                        'calib': _get_calib(calib_path)}

        with open(self.cache_path, 'wb') as handle:
            pickle.dump(cache, handle, protocol=pickle.HIGHEST_PROTOCOL)

    def _load_cache(self):
        with open(self.cache_path, 'rb') as handle:
            self.cache = pickle.load(handle)

    def _filter_cache(self):
        for t in self.cache:
            # Remove boxes not in class_dict
            self.cache[t]['boxes_2D'] = list(filter(lambda box: box.cls in self.class_to_group, self.cache[t]['boxes_2D']))
            self.cache[t]['boxes_3D'] = list(filter(lambda box: box.cls in self.class_to_group, self.cache[t]['boxes_3D']))

            # Replace class with group
            for i in range(len(self.cache[t]['boxes_2D'])):
                self.cache[t]['boxes_2D'][i].cls = self.class_to_group[self.cache[t]['boxes_2D'][i].cls]
                self.cache[t]['boxes_3D'][i].cls = self.class_to_group[self.cache[t]['boxes_3D'][i].cls]

    def get_image(self, t):
        return _get_image(os.path.join(self.img2_dir, t + ".png"))

    def get_velo(self, t, workspace_lim=((-40, 40), (-1, 2.5), (0, 70)), use_fov_filter=True):
        return _get_velo(path=os.path.join(self.velo_dir, t + '.bin'),
                         calib=self.cache[t]['calib'],
                         workspace_lim=workspace_lim,
                         use_fov_filter=use_fov_filter)

    def get_boxes_2D(self, t):
        return copy.deepcopy(self.cache[t]['boxes_2D'])

    def get_boxes_3D(self, t):
        return copy.deepcopy(self.cache[t]['boxes_3D'])

    def get_calib(self, t):
        return copy.deepcopy(self.cache[t]['calib'])

    def get_ids(self, subset):
        assert subset in ['train', 'val', 'micro', 'trainval']
        return [line.rstrip('\n') for line in open(os.path.join(self.subsets_dir, subset + '.txt'))]


def _get_image(path):
    img = Image.open(path).resize((IMG_WIDTH, IMG_HEIGHT))
    return np.asarray(img, dtype=np.float32) / 255.0


# Returns a list of dict; dict contains parameters for one object
def _read_txt_file(path):
    with open(path, 'r') as csv_file:
        reader = csv.DictReader(csv_file, delimiter=' ', fieldnames=KITTI_COLUMN_NAMES)
        txt_lbl = [row for row in reader]
    return txt_lbl


# Returns velo pts and reflectance in rectified camera coordinates
def _get_velo(path, calib, workspace_lim=((-40, 40), (-1, 2.5), (0, 70)), use_fov_filter=True):
    velo = np.fromfile(path, dtype=np.float32).reshape((-1, 4)).T
    pts = velo[0:3]
    reflectance = velo[3:]

    # Transform points from velo coordinates to rectified camera coordinates
    V2C, R0, P2 = calib
    pts = transform(np.dot(R0, V2C), pts)

    # Remove points out of workspace
    pts, reflectance = box_filter(pts, workspace_lim, decorations=reflectance)

    # Remove points not projecting onto the image plane
    if use_fov_filter:
        pts, reflectance = fov_filter(pts, P=P2, img_size=(IMG_HEIGHT, IMG_WIDTH), decorations=reflectance)

    return pts, reflectance


def _get_calib(path):
    # Read file
    with open(path, 'r') as f:
        for line in f:
            line = line.split()
            # Skip if line is empty
            if len(line) == 0:
                continue
            # Load required matrices only
            matrix_name = line[0][:-1]
            if matrix_name == 'Tr_velo_to_cam':
                V2C = np.array([float(i) for i in line[1:]]).reshape(3, 4)  # Read from file
                V2C = np.insert(V2C, 3, values=[0, 0, 0, 1], axis=0)  # Add bottom row
            elif matrix_name == 'R0_rect':
                R0 = np.array([float(i) for i in line[1:]]).reshape(3, 3)  # Read from file
                R0 = np.insert(R0, 3, values=0, axis=1)  # Pad with zeros on the right
                R0 = np.insert(R0, 3, values=[0, 0, 0, 1], axis=0)  # Add bottom row
            elif matrix_name == 'P2':
                P2 = np.array([float(i) for i in line[1:]]).reshape(3, 4)
                P2 = np.insert(P2, 3, values=[0, 0, 0, 1], axis=0)  # Add bottom row

    return V2C, R0, P2


def main():
    reader = Reader(ALL_OBJECTS, invalidate_cache=False)
    for t in np.random.choice(reader.get_ids('train'), size=3):
        boxes = reader.get_boxes_2D(t)
        pts, ref = reader.get_velo(t)
        img = reader.get_image(t)
        P2 = reader.get_calib(t)[2]

        print("Boxes:", boxes)
        print("pts.shape", pts.shape, "   ref.shape", ref.shape)
        print("img.shape", img.shape)
        print("P2.shape", P2.shape)
        print("-" * 100)


if __name__ == '__main__':
    main()
