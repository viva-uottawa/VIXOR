'''
Copyright 2019-2020 Selameab (https://github.com/Selameab)
'''

import numpy as np
from . import transforms_3D

# 2D Box declaration modes
CORNER_CORNER = 0
CORNER_DIM = 1
CENTER_DIM = 2


class Box2D:
    def __init__(self, values, mode, cls=None, confidence=None, text=None):
        self.cls = cls
        self.confidence = confidence
        self.text = text
        if mode == CORNER_CORNER:
            self.x1, self.y1, self.x2, self.y2 = values
            self.cx, self.cy, self.w, self.h = (self.x1 + self.x2) / 2, (self.y1 + self.y2) / 2, self.x2 - self.x1, self.y2 - self.y1
        elif mode == CENTER_DIM:
            self.cx, self.cy, self.w, self.h = values
            self.x1, self.y1, self.x2, self.y2 = self.cx - self.w / 2, self.cy - self.h / 2, self.cx + self.w / 2, self.cy + self.h / 2
        elif mode == CORNER_DIM:
            self.x1, self.y1, self.w, self.h = values
            self.cx, self.cy, self.x2, self.y2 = self.x1 + self.w / 2, self.y1 + self.h / 2, self.x1 + self.w, self.y1 + self.h


class Box3D:
    def __init__(self, h, w, l, x, y, z, yaw, cls=None, confidence=None, text=None):
        # Copy params
        self.h, self.w, self.l = h, w, l
        self.x, self.y, self.z = x, y, z
        self.yaw = yaw
        self.cls = cls
        self.confidence = confidence
        self.text = text


def box_to_string(box):
    s = ""
    if isinstance(box, Box2D):
        s += "Center: (%.3f, %.3f)   H,W:  (%.3f, %.3f)" % (box.cx, box.cy, box.h, box.w)
    elif isinstance(box, Box3D):
        s += "Center: (%.3f, %.3f, %.3f)   HWL: (%.3f, %.3f, %.3f)  YAW: %.3f" % (box.x, box.y, box.z, box.h, box.w, box.l, box.yaw)

    s += "" if box.cls is None else ("   Cls: " + box.cls)
    s += "" if box.confidence is None else ("   Conf: " + box.confidence)
    return s


def get_corners_3D(box):
    corners = np.array([[-box.l / 2, box.l / 2, box.l / 2, -box.l / 2, -box.l / 2, box.l / 2, box.l / 2, -box.l / 2],
                        [0, 0, 0, 0, -box.h, -box.h, -box.h, -box.h],
                        [-box.w / 2, -box.w / 2, box.w / 2, box.w / 2, -box.w / 2, -box.w / 2, box.w / 2, box.w / 2]], dtype=np.float32)
    H = np.dot(transforms_3D.translation_matrix(box.x, box.y, box.z), transforms_3D.rot_y_matrix(box.yaw))
    return transforms_3D.transform(H, corners)


def project_box_3D(P, box: Box3D) -> Box2D:
    corners = transforms_3D.project(P, get_corners_3D(box))
    x1, y1 = np.min(corners, axis=1)
    x2, y2 = np.max(corners, axis=1)
    x1, y1, x2, y2 = max(x1, 0), max(y1, 0), min(x2, 1241), min(y2, 374)
    return Box2D((x1, y1, x2, y2), mode=CORNER_CORNER)


def translate_box_3D(box, x, y, z):
    box.x += x
    box.y += y
    box.z += z
    return box
