"""
Copyright 2019-2020 Selameab (https://github.com/Selameab)
"""

import numpy as np
import tensorflow as tf

from .kitti_utils import box_filter
from .reader import Reader


def encode_batch(encoder, batch):
    if encoder is not None:
        return encoder.encode_batch(batch)
    return batch


class Generator(tf.keras.utils.Sequence):
    def __init__(self, subset, class_dict, batch_size, velo_encoder=None, boxes_3D_encoder=None, velo_augmentations=None):
        self.reader = Reader(class_dict)
        self.ids = self.reader.get_ids(subset)

        self.subset = subset
        self.class_dict = class_dict
        self.batch_size = batch_size
        self.velo_encoder = velo_encoder
        self.boxes_3D_encoder = boxes_3D_encoder
        self.velo_augmentations = velo_augmentations

    def _get_pc(self, selected_ids):
        pc_batch = []
        for i in range(len(selected_ids)):
            pc_batch += [self.reader.get_velo(selected_ids[i])]

        return encode_batch(self.velo_encoder, pc_batch)

    def get_ids(self, batch_id):
        return self.ids[batch_id * self.batch_size: (batch_id + 1) * self.batch_size]

    def __len__(self):
        return int(np.ceil(len(self.ids) / self.batch_size))

    def __getitem__(self, batch_id):
        selected_ids = self.get_ids(batch_id)
        velo_batch, boxes_3D_batch = [], []
        for i in range(len(selected_ids)):
            # Load data
            velo = self.reader.get_velo(selected_ids[i])
            boxes = self.reader.get_boxes_3D(selected_ids[i])

            # Augment
            if self.velo_augmentations is not None:
                aug_fn = np.random.choice(self.velo_augmentations)
                if aug_fn is not None:
                    (pts, ref), boxes = aug_fn(velo, boxes)
                    # Remove pts outside of workspace after augmentation
                    pts, ref = box_filter(pts, ((-40, 40), (-1, 2.5), (0, 70)), decorations=ref)
                    velo = pts, ref

            # Add to batch
            velo_batch += [velo]
            boxes_3D_batch += [boxes]

        return encode_batch(self.velo_encoder, velo_batch), encode_batch(self.boxes_3D_encoder, boxes_3D_batch)


def main():
    from .reader import CARS_ONLY
    gen = Generator('train', CARS_ONLY, batch_size=4)
    x, y = gen.__getitem__(0)
    print(x)
    print(y)


if __name__ == '__main__':
    main()
