"""
Copyright 2019-2020 Selameab (https://github.com/Selameab)
"""

import numpy as np

from . import transforms_3D
from .boxes import get_corners_3D


def box_filter(pts, box_lim, decorations=None):
    x_range, y_range, z_range = box_lim
    mask = ((pts[0] >= x_range[0]) & (pts[0] <= x_range[1]) &
            (pts[1] >= y_range[0]) & (pts[1] <= y_range[1]) &
            (pts[2] >= z_range[0]) & (pts[2] <= z_range[1]))
    pts = pts[:, mask]
    return pts if decorations is None else pts, decorations[:, mask]


# img_size = (img_height, img_width)
def fov_filter(pts, P, img_size, decorations=None):
    pts_projected = transforms_3D.project(P, pts)
    mask = ((pts_projected[0] >= 0) & (pts_projected[0] <= img_size[1]) &
            (pts_projected[1] >= 0) & (pts_projected[1] <= img_size[0]))
    pts = pts[:, mask]
    return pts if decorations is None else pts, decorations[:, mask]


def dist_bev(box_1, box_2):
    return - np.sqrt((box_1.x - box_2.x) ** 2 + (box_1.z - box_2.z) ** 2)


def nms_bev_dist(thresh, max_boxes=50, min_hit=5):
    def _nms_bev_dist(boxes):
        boxes.sort(key=lambda box: box.confidence, reverse=True)

        filtered_boxes = []
        while len(boxes) > 0 and len(filtered_boxes) < max_boxes:
            top_box = boxes[0]
            boxes = np.delete(boxes, 0)  # Remove top box from main list

            # Remove all other boxes overlapping with selected box
            boxes_to_remove = []
            hits = 0
            for box_id in range(len(boxes)):
                iou = dist_bev(boxes[box_id], top_box)
                if iou > thresh:
                    boxes_to_remove += [box_id]
                    hits += 1
            boxes = np.delete(boxes, boxes_to_remove)

            # Add box with highest confidence to output list
            if hits >= min_hit:
                filtered_boxes += [top_box]

        return filtered_boxes

    return _nms_bev_dist


def compute_mask_accurate(pts, box):
    corners = get_corners_3D(box).T
    v0P = pts.T - corners[0]
    v01 = corners[1] - corners[0]
    v03 = corners[3] - corners[0]
    v04 = corners[4] - corners[0]

    p1 = np.dot(v0P, v01)
    p3 = np.dot(v0P, v03)
    p4 = np.dot(v0P, v04)

    mask = (0 <= p1) & (p1 <= np.dot(v01, v01)) & \
           (0 <= p3) & (p3 <= np.dot(v03, v03)) & \
           (0 <= p4) & (p4 <= np.dot(v04, v04))

    return mask


def count_points_accurate(pts, box):
    return np.sum(compute_mask_accurate(pts, box))
